#include "Rules.h"


Rules::Rules(Table* d)
{
	this->d = d;

}


Rules::~Rules()
{
	
}


bool Rules::ifCardIsGood(Card c,PlayerStatus ps) {
	if (ps.CardDealt) {
		if (c.value == ps.last_card.value) return 1;
		else return 0;
	}

	int val = c.getValue();
	int col = c.getColor();
	if (war) {
		if (!(val == 0 || val == 1 || val == 11))return 0;  //nie jest waleczna
		else {
			return ((d->getTop()).sameColorOrValue(c));
		}
	}

	if (four) {
		if (!(val == 2))return 0;  //nie jest stopujaca
		else {
			return ((d->getTop()).sameColorOrValue(c));
		}
	}
	if (val==10 || d->getTop().value==10) return 1;  //dama na wszystko, wszystko na dame.
	return ((d->getTop()).sameColorOrValue(c));
}
//---------------------
bool Rules::ifAnyCardIsGood(Deck d,PlayerStatus ps) {
	for (int i = 0; i < d.size(); ++i)
		if (ifCardIsGood(d[i],ps)) return 1;
	return 0;
}