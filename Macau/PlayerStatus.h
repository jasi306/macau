#pragma once
#include "Table.h"

//zawiera status gracza.
//deklaracja potrzebna jest teraz, by moc uzyc jej w tules i interactions.
class PlayerStatus
{
private:
	int waiting;
public:
	PlayerStatus();
	
	bool isWaiting();  //cheack if player needs to wait;
	bool wait();       //waiting-- , returning bool to unlock if(wait()){} structure
	void setWaiting(int); 

	bool CardDealt;
	bool CardDrawed;
	Card last_card;
	//~PlayerStatus();
};

