#pragma once
#include "Rules.h"

//klasa interakcje odpowiedzialna za wygenerowanie dostepnych opcji na podstawie
//stolu zawartego w zasadach, zasadach, players status i deck gracza.
class Interactions
{
public:
	Interactions(PlayerStatus,Deck* ,Rules*);
	~Interactions();
	bool CzyMozePolorzyc;
	bool CzyMozeDobrac;
	bool CzyMozeZakonczycRuch;
	bool CzyMozeszDobracWojennie;
	bool CzyMozeszCzekac;
	bool CzyMozeszZaczacCzekac;

	vector<int> printOptions(Rules*);
};

