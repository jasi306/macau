#pragma once
#include "AI.h";


//klasa zawierajaca wszystko o grze.
//ma mainloop
class Game
{
public:
	Table *t;
	Rules *r;
	

	int numOfPlayers;

	void mainLoop();
	void introduction(int);
	void display(int);

	BaseAIBuild** players;
	Game(int ,int,int );
	~Game();

	void dealCards();
	void CreateCardsNames(map <int, char>&, map <int, char>&,int);
};