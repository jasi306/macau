#pragma once
#include "Interactions.h"
#include <string>

//klasa odpowiedzialna za budowanie gracza i sztucznej inteligencji.
//z wirtualnym make_move();


class BaseAIBuild {
public:
	PlayerStatus ps;
	bool alive;
	Deck deck;
	Table* dealer;
	Rules* r;
	string name;

	BaseAIBuild();
	BaseAIBuild(Table*, Rules*,string name);
	
	void ZacznijCzekaj();
	Interactions beforeMove();
	bool dealCard(int i);
	bool is_win();
	void draw();
	vector<int> findAllMachingAdresses();
	vector<vector<int>> findAllMachingCardsSetsAdresses();
	
	vector<int> findMaxSizeCardsSetAdresses(vector<vector<int>> temp);
	vector<int> findMinSizeCardsSetAdresses(vector<vector<int>> temp);

	void warDraw();
	virtual bool make_move() = 0;
};

//klasy gracza i sztucznej intelogencji.

class AIMax : public BaseAIBuild {
public:
	AIMax(Table*, Rules*, string name);
	bool make_move();
};
class AIMin : public BaseAIBuild {
public:
	AIMin(Table*, Rules*, string name);
	bool make_move();
};
class PlayerAlive : public BaseAIBuild {
public:
	PlayerAlive(Table*, Rules*, string name);
	bool tryToDealCard();
	bool make_move();
};
