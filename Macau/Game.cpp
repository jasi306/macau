#include "Game.h"

#include <string>

Game::Game(int numOfPlayers,int numOfDecks=2,int gStyle=1)
{
	this->numOfPlayers = numOfPlayers;
	this->r = r;
	map <int, char> mValue;
	map <int, char> mColor;
	CreateCardsNames(mValue, mColor , gStyle);
	t = new Table(mValue, mColor, numOfDecks);
	r = new Rules(t);
	//(d->drawCards).cards_sort();

	players = new BaseAIBuild*[numOfPlayers];
	int numOfBot = 0;
	for (int i = 0; i < numOfPlayers; ++i) {
		int choice = 0;
		
		do{
			system("cls");
			cout << "gracz " << i << endl;
			cout << "wybierz rodzaj gracza." << endl;

			cout << "1-gracz zywy\n2-strategia min(npc)\n3-strategia max(npc)\n";
			
			cin >> choice;
			
			switch (choice) {
			case 1:
				players[i] = new PlayerAlive(t, r,"");
				break;
			case 2:
				players[i] = new AIMin(t, r, "Bot (MIN) " + to_string(numOfBot++));
				break;
			case 3:
				players[i] = new AIMax(t, r, "Bot (MAX) " + to_string(numOfBot++));
				break;
			default:
				cout << "Niepoprawne dane. powtorz.\n";
				system("pause");
				break;
			}
			system("cls");
		} while ((choice < 1 || choice > 3));
	}
	dealCards();
}

void Game::dealCards() {
	for (int i = 0; i < 5; ++i) {
		for (int j = 0; j < numOfPlayers; ++j) {
		
			t->drawCards.give_top(players[j]->deck);
		}
	}
	t->drawCards.give_top(t->usedCards);
}

void Game::mainLoop() {
	int wygrany = -1;
	while (wygrany==-1) {
		for (int i = 0; i < numOfPlayers; ++i) {
			
			players[i]->beforeMove();
			bool flag=1;
			introduction(i);
			//if (players[i]->alive)introduction(i);
			while (flag) {
				if (players[i]->alive) display(i);
				flag = players[i]->make_move();
			}
			if(!players[i]->alive) system("pause");
			if (players[i]->deck.size() == 0)wygrany = i;
		}
		
	}
	cout << "wygral " << wygrany;
	return;


}


void Game::introduction(int i) {
	//system("pause");
	system("cls");
	cout << "RUCH GRACZA: " << ((players[i])->name) << endl << endl;
	system("pause");
}
void Game::display(int i) {
	
	system("cls");
	cout << "RUCH GRACZA: " << ((players[i])->name) << endl << endl;
	cout << "stol:\n  wielkosc stosu polozonego: " << t->usedCards.size() << "\n  wielkosc stosu do dobrania: " << t->drawCards.size() << endl;
	if (r->war != 0) cout << "Pula dobierania: " << r->war << endl;
	if (r->four != 0) cout << "Pula czekania: " << r->four << endl;
	cout << "\nInni gracze: \n";
	for (int j = 0; j < numOfPlayers; ++j) {
		if(j!=i)cout << "  " << players[j]->name << ": ilosc kart w talii: " << players[j]->deck.size()<<endl;
	}

	cout << "\n\n    Gorna karta: " << t->getTop() << endl << endl;
	cout << "twoje karty: \n";
	cout << players[i]->deck << endl<<endl;
	return;
}



Game::~Game()
{
	for (int i = 0; i < numOfPlayers; ++i) {
		delete players[i];
	}
	delete[] players;
}





void Game::CreateCardsNames(map <int, char> &mValue, map <int, char> &mColor,int gStyle) {
	mValue[0] = '2';
	mValue[1] = '3';
	mValue[2] = '4';
	mValue[3] = '5';
	mValue[4] = '6';
	mValue[5] = '7';
	mValue[6] = '8';
	mValue[7] = '9';
	mValue[8] = '0';
	mValue[9] = 'J';
	mValue[10] = 'Q';
	mValue[11] = 'K';
	mValue[12] = 'A';
	//mValue[13]='Jocker';

	if (gStyle) {
		mColor[0] = 'H';
		mColor[1] = 'T';
		mColor[2] = 'C';
		mColor[3] = 'P';
	}
	else {
		mColor[0] = 3;
		mColor[1] = 4;
		mColor[2] = 5;
		mColor[3] = 6;
	}
}