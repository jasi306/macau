#pragma once
#include "Deck.h"
#include <map>


//klasa Stol zawiera stos kart do dobrania i stos kart odrzuconych.
// + sprawdz, ostatnia polozona (getTop()) + wtasuj + generuj karty w konstruktorze.
class Table
{
public:
	Deck drawCards;
	Deck usedCards;

	bool check_if_drawable_if_needed_shuffle();
	Card getTop();
	void add_cards();
	void add_cards_without_last();
	
		/*static Singleton* get_instance(){
			if(instance == NULL)
				instance = new Singleton();

			return instance;
		}


	private:
		Singleton *instance = NULL;*/

	Table(map <int, char> mapedColor, map <int, char> mapedValue,int numerOfDeck);
};

	

