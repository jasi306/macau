#include "Interactions.h"



Interactions::Interactions(PlayerStatus ps, Deck* deck, Rules* r)
{
	CzyMozeszCzekac = ps.isWaiting();
	CzyMozeszDobracWojennie = (r->war && !ps.CardDealt) && !ps.isWaiting() && !(r->four && !ps.CardDealt);
	CzyMozePolorzyc = r->ifAnyCardIsGood(*deck,ps) && !ps.isWaiting() && !ps.CardDrawed;
	CzyMozeDobrac = !(ps.CardDealt || ps.CardDrawed) && !(r->war && !ps.CardDealt) && !ps.isWaiting() && !(r->four && !ps.CardDealt);  //dobranie karty
	CzyMozeZakonczycRuch = (ps.CardDealt || ps.CardDrawed) && !(r->war && !ps.CardDealt) && !ps.isWaiting() && !(r->four && !ps.CardDealt);
	CzyMozeszZaczacCzekac = (r->four && !ps.CardDealt) && !ps.isWaiting();  //zamienia czworki na waity
}


Interactions::~Interactions()
{

}
vector<int> Interactions::printOptions(Rules* r) {
	vector<int> out;
	int i = 0;
	if (CzyMozePolorzyc) 
	{ 
		cout << i++ << ":Poloz" << endl; 
		out.push_back(0);
	}
	if (CzyMozeDobrac) 
	{ 
		cout << i++ << ":Dobierz" << endl;
		out.push_back(1);
	}
	if (CzyMozeszCzekac) 
	{ 
		cout << i++ << ":Czekaj" << endl;
		out.push_back(2);
	}
	if (CzyMozeszZaczacCzekac)
	{
		cout << i++ << ":zacznij czekac" << endl;
		out.push_back(3);
	}
	if (CzyMozeszDobracWojennie) 
	{ 
		cout << i++ << ":Dobierz " << r->war << endl;
		out.push_back(4);
	}
	if (CzyMozeZakonczycRuch)
	{
		cout << i++ << ":Zakoncz Ruch" << endl;
		out.push_back(5);
	}
	cout << i++ << ":Posortuj" << endl;
	out.push_back(6); //posortuj.
	return out;
}