#include "Deck.h"
#include <algorithm>

Card& Deck::operator[](const int i)
{
	return Cards[i];
}

const Card& Deck::operator[](const int i) const
{
	return Cards[i];
}


ostream& operator<<(ostream& out, const Deck& deck)
{
	int size = deck.Cards.size();
	for (int i = 0; i < size; ++i) {
		out << deck.Cards[i] << " ";
	}
	return out;
}


void Deck::add(Card card) {
	Cards.push_back(card);
}


void Deck::add(Deck IDeck) {
	for (int i = 0; i < IDeck.Cards.size(); ++i)
		add(IDeck.Cards[i]);
}
void Deck::add_without_last(Deck IDeck) {
	for (int i = 0; i < IDeck.Cards.size() - 1; ++i)
		add(IDeck.Cards[i]);
}

void Deck::randomize() {
	vector<Card> New;
	int Random;
	for (int i = 0; Cards.size() > 0; ++i) {
		Random = rand() % (Cards.size());
		New.push_back(Cards[Random]);
		Cards.erase(Cards.begin() + Random);
	}
	Cards = New;
}

Deck Deck::cards_sort() {
	sort(Cards.begin(),Cards.end());
	return *this;
	/*Deck New;
	vector<Card> Temp = CARDS;

	for (int i = 0; Temp.size(); ++i) { 
		int localmin_position = 0;
		Card localmin = Temp[0];
		for (unsigned int ii = 1; ii < Temp.size(); ++ii) {
			if (Temp[localmin_position].value < Temp[ii].value) {
				localmin_position = ii;
	
			}
			else if (Temp[localmin_position].value == Temp[ii].value) {
				if (Temp[localmin_position].color < Temp[ii].color) {
					localmin_position = ii;
				}
			}

		}
		New.CARDS.push_back(Temp[localmin_position]);
		Temp.erase(Temp.begin() + localmin_position);
	}
	return New;*/
}





void Deck::print() {
	for (int i = 0; i < Cards.size(); ++i) {
		cout << Cards[i] << " ";
	}
	cout<<endl<<" s="<<Cards.size()<<endl;
}

Card Deck::top() {
	return Cards.back();
}


void Deck::erase_top() {
	Cards.pop_back();
}


void Deck::Erase(int possition) {
	Cards.erase(Cards.begin() + possition);
}

void Deck::give_top(Deck &addressee) {
	addressee.add(top());
	erase_top();
}

void Deck::give(Deck &addressee, int possition) {
	addressee.add(this->Cards[possition]);
	Erase(possition);
}

void Deck::give_all(Deck &addressee) {
	while (!Cards.empty()) {
		give_top(addressee);
	}
}

int Deck::size() {
	return Cards.size();
}

void Deck::clear() {
	Cards.clear();
}


void Deck::clear_without_last() {
	while (size() > 1) {
		Erase(0);
	}
}