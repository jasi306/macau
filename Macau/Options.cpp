#include "Options.h"

Options::Options() {
	symbolsType = 1;
	numOfDecks = 2;
}

bool Options::load() {
	
	fstream file ("options",ios::in);
	if(!file.good() || file.eof()) return 0;
	file >> symbolsType;
	file >> numOfDecks;
	file.close();
}
void Options::save() {
	fstream file("options", ios::out);
	if (!file.is_open()) throw domain_error("Blad zapisu opcji. Opcje nie zostana zapisane.");
	file << symbolsType<<' '<< numOfDecks;
	file.close();

}
void Options::ask() {
	cout << "nie wykryto pliku opcji, badz jest on uszkodzony.\n";
	cout << "wybierz przedstawienie koloru kart\n0:";
	cout << " " << char(3) << " " << char(4) << " " << char(5) << " " << char(6) << endl;
	cout << "1: " << 'H' << " " << 'T' << " " << 'C' << " " << 'P' << endl;
	cin >> symbolsType;
	numOfDecks = 2;
}


ostream& operator<<(ostream& out, const Options& o) {
	out << "Aktualne opcje:\n";
	out << "wyswietlanie koloru: ";
	if (o.symbolsType)
		out << "Literowe [H,T,C,P]\n";
	else
		out << "Symboliczne [" << char(3) << "," << char(4) << "," << char(5) << "," << char(6) << "]" << endl;
	out  << "Ilosc talii w grze: " << o.numOfDecks << endl;
	return out;
}
void Options::optionsLoop() {
	bool flag=1;
	while (flag) {
		system("cls");
		cout << *this;
		int choice = 0;
		cout << "\n0 - zmien sposob wyswietlania.\n1 - zmien ilosc talli w grze\n2 - wyjdz\n";
		cin >> choice;
		switch(choice) {
			case 0:
				symbolsType = !symbolsType;
				break;
			case 1:
				int temp;
				cout << "Ile talli ma byc w grze? (1-20)\n" ;
				cin >> temp;
				if (temp > 0 && temp < 20)
					numOfDecks = temp;
				else {
					cout << "bledna ilosc talli";
					system("pause");
				}
				break;
			case 2:
				save();
				flag = 0;
				break;
			default:
				cout << "bledna opcja";
				system("pause");
		}
	}
	
}