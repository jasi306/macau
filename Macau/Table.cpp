#include "Table.h"

Card Table::getTop() {
	return usedCards.Cards.back();
}

bool Table::check_if_drawable_if_needed_shuffle() {
	if (drawCards.size() < 1) {

		Deck temp = usedCards;
		usedCards.clear();
		temp.give_top(usedCards);
		temp.give_all(drawCards);
		drawCards.randomize();
	}
	if (drawCards.size() < 1) {
		return 0;
	}
	return 1;
}

void Table::add_cards() {
	drawCards.add(usedCards);
	//UsedCards.give_all(cards);
	drawCards.randomize();
	usedCards.clear();
}
void Table::add_cards_without_last() {
	drawCards.add_without_last(usedCards);
	drawCards.randomize();
	usedCards.clear_without_last();
}

/*static Singleton* get_instance(){
	if(instance == NULL)
		instance = new Singleton();

	return instance;
}


private:
	Singleton *instance = NULL;*/

Table::Table(map <int, char> mapedValue, map <int, char> mapedColor,int numerOfDeck) {

	for (int j = 0; j < numerOfDeck; ++j) {
		for (int i = 0; i < 13; ++i) {
			for (int ii = 0; ii < 4; ++ii) {
				drawCards.add(Card(i, ii, mapedValue[i], mapedColor[ii]));
			}
		}
	}
	drawCards.randomize();
}

