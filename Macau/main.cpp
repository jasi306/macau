#define _CRT_SECURE_NO_WARNINGS
#include <fstream>

#include "Game.h"
#include "Options.h"


using namespace std;


void showRules();
int main() {
	srand(time(NULL));

	int choice;

	system("cls");
	
	Options o;
	try {
		if (!o.load()) {
			o.ask();
			o.save();
		}
	}
	catch (domain_error & de) {
		cerr << "domain_error: " << de.what()<<endl;
		system("pause");
	}
	
	while (1) {
		int choice;
		system("cls");
		cout << "1 - rozpocznij gre.\n2 - pokaz zasady\n3 - opcje\n4 - wyjdz.\n";
			cin >> choice;
			switch (choice) {
			case 1: {
				int i=-1;
				while (1) {
					system("cls");
					cout << "ile graczy? (2-8)\n";
					cin >> i;
					if (i > 1 && i < 9) break;
					cout << "gra przewidziana jest dla od 2 do 8 graczy."<<endl;
					system("pause");
				}
				
				
				Game game(i, o.numOfDecks, o.symbolsType);  //druga zmienna, ilosc deckow
				try {
					game.mainLoop();
				}
				catch (exception &e) {
					cout << e.what();
				}
				break;
			}
			case 2:
				showRules();
				break;
			case 3:
				o.optionsLoop();
				
				break;
			case 4:
				return 0;
				break;
			default:
				cout << "Nie ma takiej opcji:" << choice<<endl;
			}
	}
	return -1;
}
void showRules()
 {
	system("cls");
	fstream file("rules.txt",ios::in);
	string line;
	while (getline(file, line)) {
		cout << line << endl;
	}
	system("pause");
}