#include "AI.h"
#pragma region BaseAIBuild



void BaseAIBuild::ZacznijCzekaj() {
	ps.setWaiting(r->four);
	r->four = 0;
}
void BaseAIBuild::warDraw() {
	r->war++;
	while (--r->war) draw();
}

Interactions BaseAIBuild::beforeMove() {
	ps.CardDealt = 0;
	ps.CardDrawed = 0;
	return Interactions(ps, &deck, r);
}

BaseAIBuild::BaseAIBuild(Table* dealer, Rules* r, string name) {
	this->dealer = dealer; //znaj swojego dilera
	this->r = r;
	this->name = name;
}

bool BaseAIBuild::is_win() {
	if (deck.size() == 0) {
		return true;
	}
	else return false;
}

bool BaseAIBuild::dealCard(int i) {

	if (r->ifCardIsGood(deck[i], ps)) {
		
		//obsluga kart funkcyjnych
		switch (deck[i].value)
		{
		case 0:
			r->war += 2;
			break;
		case 1:
			r->war += 3;
			break;
		case 11:
			r->war += 5;
			break;
		case 2:
			r->four += 1;
			break;
		default:
			break;
		}
		//statusy
		ps.last_card = deck[i];
		ps.CardDealt = 1;

		deck.give(dealer->usedCards, i);
		return 1;
	}
	return 0;
}


//void BaseAIBuild::play(Deck cards) {
//
//	for (int i = 0; i < cards.size(); ++i) {
//		for (int i = 0; i < deck.size(); ++i) {
//			if (deck[i].color == cards.top().color  && deck[i].value == cards.top().value) {
//				deck.Erase(i);
//			}
//		}
//		cards.give_top(dealer->usedCards);
//	}
//}
void BaseAIBuild::draw() {
	if (dealer->check_if_drawable_if_needed_shuffle()) {
		dealer->drawCards.give_top(deck);
		
	}
	else {
		cout<<"Wszystkie karty sa na rekach graczy.\n";
	}

	ps.CardDrawed = 1;
}

vector<int> BaseAIBuild::findAllMachingAdresses() {
	vector<int> matching;
	for (int i = 0; i < deck.size(); ++i) {
		/*
		if (dealer->usedCards.top().value == deck.Cards[i].value ||
			dealer->usedCards.top().color == deck.Cards[i].color) {
			matching.add(deck.Cards[i]);*/
			
			if (r->ifCardIsGood(deck[i], ps))
				matching.push_back(i);
		//}
	}
	return matching;
}

vector<vector<int>> BaseAIBuild::findAllMachingCardsSetsAdresses() {
	vector<int> temp = findAllMachingAdresses();
	vector<vector<int>> decks;
	for (int i = 0; i < temp.size(); ++i) {
		vector<int> a;
		a.push_back(temp[i]);  //gwarantuje, �e set zaczyna sie od pasujacej.

		for (int ii = 0; ii < deck.size(); ++ii) {
			/*if (deck.Cards[ii].value == temp.Cards[i].value) {
				decks.back().add(deck.Cards[ii]);
			}*/
			if (deck[ii].value == deck[temp[i]].value && i!=ii) {  //i!=ii, brak powtorek. 
				a.push_back(ii);
			}
		}
		decks.push_back(a);
	}
	return decks;
}


vector<int> BaseAIBuild::findMinSizeCardsSetAdresses( vector<vector<int>> temp) {
	int Min = 100, position = -1;
	for (int i = 0; i < temp.size(); ++i) {
		if (temp[i].size() < Min) {
			Min = temp[i].size();
			position = i;
		}
	}
	return temp[position];
}

vector<int> BaseAIBuild::findMaxSizeCardsSetAdresses(vector<vector<int>> temp) {
	int Max = 0, position = -1;
	for (int i = 0; i < temp.size(); ++i) {
		if (temp[i].size() + 1 > Max) {
			Max = temp[i].size() + 1;
			position = i;
		}
	}
	return temp[position];
}


#pragma endregion

#pragma region PlayerAlive
PlayerAlive::PlayerAlive(Table* d, Rules* r, string name) : BaseAIBuild(d,r, name){
	alive = 1;
	cout << "jak masz na imie?" << endl;
	cin >> this->name;
}

bool PlayerAlive::tryToDealCard() {
	
	int choice = 0;
	cout << "ktora karte poloczyc?" << endl;
	cout << "gorna:" << dealer->getTop()<<endl;
	for (int i = 0; i < deck.size(); ++i) {
		cout<<i<<":"<<deck[i]<<" ";
	}
	cout << endl;
	cin >> choice;
	return dealCard(choice);
	
}

bool PlayerAlive::make_move() {
	Interactions i(ps, &deck, r);
	vector<int> choiceMap = i.printOptions(r);

	int choice = 0;
	cin >> choice;
	choice = choiceMap[choice];

	switch (choice) {
	case 0:
		if (!tryToDealCard()) {
			cout << "Niepoprawny ruch. Try again.\n";
			system("pause");
		}
		return 1;
		break;
	case 1:
		draw();
		break;
	case 2:
		this->ps.wait();
		return 0;
		break;
	case 3:
		ZacznijCzekaj();
		return 0;
		break;
	case 4:
		warDraw();
		return 0;
		break;
	case 5:
		return 0;
		break;
	case 6:
		deck.cards_sort();
		return 1;
	}
	return 1;
}

#pragma endregion

#pragma region AIMax
AIMax::AIMax(Table* d, Rules* r, string name) : BaseAIBuild(d, r, name) {
	alive = 0;
}

bool AIMax::make_move(){
	Interactions i(ps, &deck, r);
	if (deck.size() == 0) return 0; //wygrales
	if (i.CzyMozePolorzyc) {
		int index = findMaxSizeCardsSetAdresses(findAllMachingCardsSetsAdresses())[0];
		cout << "AI " << name << " kladzie: " << deck[index] << endl;
		if (!dealCard(index)) throw exception("AI kladzie nieprawidlowa karte.");
	}
	else if (i.CzyMozeszCzekac) {
		cout << "AI " << name << " czeka\n";
		this->ps.wait();
		return 0;
	}
	else if (i.CzyMozeszDobracWojennie) {
		cout << "AI " << name << " dobiera " << r->war << "\n";
		warDraw();
		return 0;
	}
	else if (i.CzyMozeDobrac) {
		cout << "AI " << name << " dobiera\n";
		draw();
	}
	else if (i.CzyMozeszZaczacCzekac) {
		cout << "AI " << name << " zaczyna czekac\n";
		ZacznijCzekaj();
		return 0;
	}
	else if (i.CzyMozeZakonczycRuch) {
		cout << "AI " << name << " konczy ruch\n";
		return 0;
	}
	return 1;
		/*Interactions i(ps, &deck, r);
		if (i.CzyMozePolorzyc) {
			if (!dealCard(findMaxSizeCardsSetAdresses(findAllMachingCardsSetsAdresses())[0])) throw exception("AI kladzie nieprawidlowa karte.");
		}
		else if (i.CzyMozeszCzekac) this->ps.wait();
		else if (i.CzyMozeszDobracWojennie) warDraw();
		else if (i.CzyMozeDobrac) draw();
		else if (i.CzyMozeszZaczacCzekac) ZacznijCzekaj();
		else if (i.CzyMozeZakonczycRuch) return 0;
		return 1;*/
}
#pragma endregion

#pragma region AIMin
AIMin::AIMin(Table* d, Rules* r, string name) : BaseAIBuild(d, r, name) {
	alive = 0;
}
bool AIMin::make_move() {
	Interactions i(ps, &deck, r);
	if (deck.size() == 0) return 0; //wygrales
	if (i.CzyMozePolorzyc) {
		int index = findMinSizeCardsSetAdresses(findAllMachingCardsSetsAdresses())[0];
		cout << "AI " << name << " kladzie: " << deck[index]<<endl;
		if (!dealCard(index)) throw exception("AI kladzie nieprawidlowa karte.");
	}
	else if (i.CzyMozeszCzekac) {
		cout << "AI " << name << " czeka\n";
		this->ps.wait();
		return 0;
	}
	else if (i.CzyMozeszDobracWojennie) {
		cout << "AI "<<name<<" dobiera "<<r->war<<"\n";
		warDraw();
		return 0;
	}
	else if (i.CzyMozeDobrac) {
		cout << "AI " << name << " dobiera\n";
		draw();
	}
	else if (i.CzyMozeszZaczacCzekac) {
		cout << "AI " << name << " zaczyna czekac\n";
		ZacznijCzekaj();
		return 0;
	}
	else if (i.CzyMozeZakonczycRuch) {
		cout << "AI " << name << " konczy ruch\n";
		return 0;
	}
	return 1;

	/*vector<Deck> temp = findAllMachingCardsSetsAdresses();

	if (temp.size() == 0) {
		draw();
	}
	else {
		UseYourStrategy(temp);
	}*/
}
#pragma endregion


