#pragma once
#include <fstream>
#include <iostream>
using namespace std;
class Options
{
public:
	int symbolsType;
	int numOfDecks;
	Options();
	void save();
	bool load();
	void ask();
	void optionsLoop();
	friend ostream& operator<<(ostream&, const Options&);
};

