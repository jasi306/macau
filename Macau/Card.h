#pragma once
#include <iostream>



//klasa Karta stanowiaca podstawe kazdej gry karcianej.
//zawiera informacje o kolorze,wartosci. Oraz podstawowe operatory.
using namespace std;
class Card
{
public:

	
	char mColor;  //do wypisywania koloru
	char mValue;  //do wypisywania wartosci
	int color; 
	int value; 
	
	~Card();
	Card();
	Card(int,int,char,char);
	int getValue();
	int getColor();

	bool operator>(const Card& c);
	bool operator<(const Card& c);
	bool operator>=(const Card& c);
	bool operator<=(const Card& c);
	bool operator==(const Card& c);

	friend ostream& operator<<(ostream&, const Card&);
	friend istream& operator>>(istream&, Card&);

	bool sameColor(const Card& c);
	bool sameValue(const Card& c);
	bool sameColorOrValue(const Card& c);
};







